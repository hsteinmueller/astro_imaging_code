import numpy as np
import astropy.units as u

from astropy.time import Time
from astropy.coordinates import Angle


def dms_to_deg(d, m, s):
    return Angle((d, m, s), unit=u.deg).degree


def hms_to_deg(h, m, s):
    return Angle((h, m, s), unit="hourangle").degree


def earth_rotation_angle(time_jd):
    DU = time_jd - 2451545.0
    theta_deg = 360 * (0.7790572732640 + 1.00273781191135448 * DU)
    return theta_deg % 360


def rotation_matrix_z(theta_prime_deg):
    theta_prime_rad = np.deg2rad(theta_prime_deg)
    return np.array(
        [
            [np.cos(theta_prime_rad), np.sin(theta_prime_rad), 0],
            [-np.sin(theta_prime_rad), np.cos(theta_prime_rad), 0],
            [0, 0, 1],
        ]
    )


def rotation_matrix_y(phi_deg):
    angle = np.deg2rad(90 - phi_deg)
    return np.array(
        [
            [np.cos(angle), 0, -np.sin(angle)],
            [0, 1, 0],
            [np.sin(angle), 0, np.cos(angle)],
        ]
    )


def compute_azimut_and_height(time_jd, ra_deg, dec_deg):
    alpha_deg, delta_deg = ra_deg, dec_deg
    lambda_deg, phi_deg = dms_to_deg(10, 53, 22), dms_to_deg(
        49, 53, 6
    )  # sternwarte bamberg

    theta_deg = earth_rotation_angle(time_jd)
    theta_prime_deg = theta_deg + lambda_deg

    rot_z = rotation_matrix_z(theta_prime_deg)
    rot_y = rotation_matrix_y(phi_deg)

    alpha_rad, delta_rad = np.deg2rad(alpha_deg), np.deg2rad(delta_deg)
    r = np.array(
        [
            np.cos(alpha_rad) * np.cos(delta_rad),
            np.sin(alpha_rad) * np.cos(delta_rad),
            np.sin(delta_rad),
        ]
    )
    x = np.diag((-1, 1, 1)) @ (rot_y @ (rot_z @ r))

    # r = cos(h), y = x[1], x = x[0] => t = arctan2(y, x)
    azimut_angle_rad = np.arctan2(x[1], x[0])
    if azimut_angle_rad < 0.0:
        azimut_angle_rad = 2.0 * np.pi + azimut_angle_rad
    azimut_angle = Angle(azimut_angle_rad, unit=u.rad)
    height_angle = Angle(np.arcsin(x[2]), unit=u.rad)

    return azimut_angle, height_angle, theta_deg


objs = {
    "IC434": [hms_to_deg(5, 41, 0.9), dms_to_deg(-2, 27, 14)],
    "IC1396": [hms_to_deg(21, 38, 58), dms_to_deg(59, 29, 18)],
    "IC1805": [hms_to_deg(2, 33, 22), dms_to_deg(61, 26, 36)],
    "M1": [hms_to_deg(5, 34, 31.94), dms_to_deg(22, 0, 52.2)],
    "M13": [hms_to_deg(16, 41, 41.24), dms_to_deg(36, 27, 35.5)],
    "M16": [hms_to_deg(18, 18, 48), dms_to_deg(-13, 49, 0)],
    "M20": [hms_to_deg(18, 2, 23), dms_to_deg(-23, 1, 48)],
    "M31": [hms_to_deg(0, 42, 44.3), dms_to_deg(41, 16, 9)],
    "M33": [hms_to_deg(1, 33, 50.02), dms_to_deg(30, 39, 36.7)],
    "M42": [hms_to_deg(5, 35, 17.3), dms_to_deg(-5, 23, 28)],
    "M45": [hms_to_deg(3, 47, 24), dms_to_deg(24, 7, 0)],
    "M51": [hms_to_deg(13, 29, 52.7), dms_to_deg(47, 11, 43)],
    "M57": [hms_to_deg(18, 53, 35.079), dms_to_deg(33, 1, 45.03)],
    "M81": [hms_to_deg(9, 55, 33.2), dms_to_deg(69, 3, 55)],
    "NGC281": [hms_to_deg(0, 52, 59.3), dms_to_deg(56, 37, 19)],
    "NGC5139": [hms_to_deg(13, 26, 47.28), dms_to_deg(-47, 28, 46.1)],
    "NGC7023": [hms_to_deg(21, 1, 35.6), dms_to_deg(68, 10, 10)],
    "NGC7283": [hms_to_deg(22, 28, 32.7), dms_to_deg(17, 28, 13)],
    "NGC7635": [hms_to_deg(23, 20, 48.3), dms_to_deg(61, 12, 6)],
    "nCarinae": [hms_to_deg(6, 34, 58.57993), dms_to_deg(52, 58, 32.1919)],
    "aPavonis": [hms_to_deg(20, 25, 38.85705), dms_to_deg(-56, 44, 6.323)],
    "Albireo": [hms_to_deg(19, 30, 43.286), dms_to_deg(27, 57, 34.84)],
    "dCygniAB": [hms_to_deg(19, 44, 58.47854), dms_to_deg(45, 7, 50.9161)],
    "GroMagWolke": [hms_to_deg(5, 24, 0), dms_to_deg(-69, 48, 0)],
    "SchleierNebel": [hms_to_deg(20, 45, 38), dms_to_deg(30, 42, 30)],
}

times = [
    Time("2021-01-11T18:0:0.0", format="isot", scale="utc"),
    Time("2021-01-12T18:0:0.0", format="isot", scale="utc"),
    Time("2021-01-13T18:0:0.0", format="isot", scale="utc"),
    Time("2021-01-14T18:0:0.0", format="isot", scale="utc"),
    Time("2021-01-15T18:0:0.0", format="isot", scale="utc"),
]

if __name__ == "__main__":
    for time in times:
        print(f"\n========================\n{time}\n")
        for (k, v) in objs.items():
            azimut_angle, height_angle, era_deg = compute_azimut_and_height(
                time.jd, v[0], v[1]
            )
            print(k)
            print(
                f"azimut:\t{azimut_angle.to_string(u.degree)}\nheight:\t{height_angle.to_string(u.degree)}\nera:\t{era_deg}\n"
            )

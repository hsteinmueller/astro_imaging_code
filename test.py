import astropy.units as u


from astropy.time import Time
from astropy.coordinates import Angle
from main import (
    compute_azimut_and_height,
    dms_to_deg,
    hms_to_deg,
)
from pytest import approx


DELTA = 1e-3


def test_1():
    """
    Input:
        day: 2021-02-07
        time: 22:23:24.0 (UTC)
        location: Remeis observatory (see Praktikumsanleitung)
        Right ascension of the object (RA): 18h18m48s
        Declination of the object (DEC): -13d48m24s

    Output:
        ERA (def. GMST): 113.8368 deg
        Azimut: 44d48m25.25s
        Elevation: -46d24m20.47s
    """
    time = Time("2021-02-07T22:23:24.0", format="isot", scale="utc")  # yyyy-mm-dd
    time_jd = time.jd
    ra_deg = hms_to_deg(18, 18, 48)
    dec_deg = dms_to_deg(-13, 48, 24)

    azimut_angle, height_angle, era_deg = compute_azimut_and_height(
        time_jd, ra_deg, dec_deg
    )

    assert era_deg == approx(113.8368, DELTA)
    assert azimut_angle.dms == approx(Angle((44, 48, 25.25), unit=u.deg).dms, DELTA)
    assert height_angle.dms == approx(Angle((-46, 24, 20.47), unit=u.deg).dms, DELTA)


def test_2():
    """
    Input:
        day: 2021-01-07
        time: 00:00:00.0
        RA = 02h31m49.09s
        DEC = 89d15m50.8s

    Output:
        ERA(def. GMST): 106.5133 deg
        Azimut: 358d52m26.91s
        Elevation: 50d00m51.55s
    """
    time = Time("2021-01-07T00:00:0.0", format="isot", scale="utc")  # yyyy-mm-dd
    time_jd = time.jd
    ra_deg = hms_to_deg(2, 31, 49.09)
    dec_deg = dms_to_deg(89, 15, 50.8)

    azimut_angle, height_angle, era_deg = compute_azimut_and_height(
        time_jd, ra_deg, dec_deg
    )

    assert era_deg == approx(106.5133, DELTA)
    assert azimut_angle.dms == approx(Angle((358, 52, 26.91), unit=u.deg).dms, DELTA)
    assert height_angle.dms == approx(Angle((50, 00, 51.55), unit=u.deg).dms, DELTA)
